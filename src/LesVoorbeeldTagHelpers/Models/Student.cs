﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LesVoorbeeldTagHelpers.Models
{
    public enum Graad
        {
            Voldoende,
            Onderscheiding
        }
    public class Student
    {
        [Display(Name="Nummer")]
        [Required(ErrorMessage ="Gelieve een nummer op te geven")]
        [Range(typeof(int), "100", "999", ErrorMessage = "Het nummer moet een waarde hebben tussen {1} en {2}")]
        public int Id { get; set; }

        [Display(Name="Naam")]
        [Required(ErrorMessage ="Gelieve een naam op te geven")]
        [MinLength(3,ErrorMessage = "De naam moet minstens {1} tekens langs zijn"), MaxLength(35)]
        public string Naam { get; set; }

        [Display(Name="Graad")]
        public Graad AfstudeerGraad { get; set; }


        public string[] Graden() {
            return Enum.GetNames(typeof(Graad));
        }

        public override string ToString()
        {
            return this.GetType().Name;
        }
    }
}
